import ffmpeg
import librosa
import IPython.display as ipd 
from random import randint, random

from pyAudioAnalysis import audioBasicIO, audioFeatureExtraction
import os
from scipy.stats._continuous_distns import rdist
from librosa import beat

DossierSource = 'Rushs/'

# ~ for i in range(len(list)):
	# ~ ext = list[i].split('.')[-1]
	# ~ print(ext)
	# ~ os.rename('Rushs/'+ list[i], 'Rushs/rush' + str(i))
	
def getBPM(title):
	[Fs, x] = audioBasicIO.readAudioFile(DossierSource + title)
	F, f_names = audioFeatureExtraction.stFeatureExtraction(x[:,1], Fs, 0.050*Fs, 0.025*Fs);
	BPM = audioFeatureExtraction.beatExtraction(F, 1)
	return BPM

def getBeatTimes(fichier):
	# read audio file 
	x, sr = librosa.load(fichier) 
	ipd.Audio(x, rate=sr)

	# approach 1 - onset detection and dynamic programming
	tempo, beat_times = librosa.beat.beat_track(x, sr=sr, start_bpm=60, units='time')

	clicks = librosa.clicks(beat_times, sr=sr, length=len(x))
	ipd.Audio(x + clicks, rate=sr)
	return beat_times

def rushSelection(listRushs, listUsedRushs):
	rd = randint(0, len(listRushs)-  1)
	print(rd)
	print(listRushs[rd])
	currentVid = ffmpeg.input(DossierSource + listRushs[rd])
	listUsedRushs.append(listRushs[rd])
	listRushs.pop(rd)
	trimming(currentVid, beatTimes[i], beatTimes[i+1])
	return currentVid


def trimming(video, _start, _finish):
	ffmpeg.trim(video, start = _start, finish = _finish, duration = _finish - _start)

list = os.listdir(DossierSource)	

nameMusic = None
listRushs = []
#Tri des fichiers
for i in list:
	print(DossierSource + i)
	if (i.endswith('.wav') or i.endswith('.mp3')):
		nameMusic = i
	if (i.endswith('.MTS') or i.endswith('.mp4') or i.endswith('.avi')):
		listRushs.append(i)
				
#Analyse de la musique
if nameMusic != None :
	BPM = getBPM(nameMusic)
	beatTimes = getBeatTimes(nameMusic)
	
else:
	raise ValueError("Pas de musique en .wav ou .mp3 trouvée")
#BPM contient le tempo de la musique (ne fonctionne pas bien)
#beatTimes contient les moments forts de la musique, on s'en servira comme point de changement de rush


#Choix du premier rush

###TODO Créer fonction qui sélectionne automatiquement un rush et l'importe à partir de la listRushs
rd = randint(0, len(listRushs)-1)
print(rd)
print(len(listRushs))
video = ffmpeg.input(DossierSource + listRushs[rd])
v = ffmpeg.probe(DossierSource + listRushs[rd])
listUsedRushs = [listRushs[rd]]
listRushs.pop(rd)
trimming(video, 0, beatTimes[0])
print(type(video))

for i in range(len(beatTimes) - 1):
	#Test probabilité changement rush
	if(random() <= 0.4):
		if(len(listRushs) > 0):
# 			rd = randint(0, len(listRushs)-  1)
# 			currentVid = ffmpeg.input(DossierSource + listRushs[rd])
# 			listUsedRushs.append(listRushs[rd])
# 			listRushs.pop(rd)
# 			trimming(currentVid, beatTimes[i], beatTimes[i+1])
			print(i)
			currentVid = rushSelection(listRushs, listUsedRushs)
			print(type(currentVid))
			ffmpeg.concat(video, currentVid, v=1, a=1)
		else:
			listRushs = listUsedRushs
			listUsedRushs = []
	

audio = ffmpeg.input(DossierSource + nameMusic)
stream = ffmpeg.output(video, 'output.mp4')
  
ffmpeg.run(stream)



# v1 = ffmpeg.input(DossierSource + 'rush0')
# v2 = ffmpeg.input(DossierSource + 'rush1')
# stream = ffmpeg.concat(v1, v2, v=1, a=1)
#  
# stream = ffmpeg.output(stream, 'output.mp4')
#  
# ffmpeg.run(stream)




