import ffmpeg
from pyAudioAnalysis import audioBasicIO, audioFeatureExtraction
import os

DossierSource = 'Rushs/'

def getBPM(title):
	[Fs, x] = audioBasicIO.readAudioFile(DossierSource + title)
	F, f_names = audioFeatureExtraction.stFeatureExtraction(x[:,1], Fs, 0.050*Fs, 0.025*Fs);
	print(F)
	print(f_names)
	print(len(F))
	BPM = audioFeatureExtraction.beatExtraction(F, 50)
	print(BPM)
	return BPM

list = os.listdir(DossierSource)
nameMusic = None
for i in list:
	print(DossierSource + i)
	if (i.endswith('.wav') or i.endswith('.mp3')):
		nameMusic = i
if nameMusic != None :
	BPM = getBPM(nameMusic)
else:
	raise ValueError("Pas de musique en .wav ou .mp3 trouvée")


